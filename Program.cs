﻿using VkNet;
using VkNet.Model;
using VkNet.Model.RequestParams;


VkApi vk_api = null;


async Task<string> send_post_request(string url, IEnumerable<KeyValuePair<string, string>> post_data) {

    using (var httpClient = new HttpClient()) {

        using (var content = new FormUrlEncodedContent(post_data)) {

            content.Headers.Clear();
            content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

            HttpResponseMessage response = await httpClient.PostAsync(url, content);

            return await response.Content.ReadAsStringAsync();
        }
    }
}


do {

    while (!Console.KeyAvailable) {

        string token, peer_id, count;


        if (vk_api is null) {

            Console.Write("enter vk api token: ");
            token = Console.ReadLine();

            vk_api = new VkApi();

            vk_api.Authorize(new ApiAuthParams {

                AccessToken = token
            });
        }

        Console.Write("enter peer id: ");
        peer_id = Console.ReadLine();

        Console.Write("enter count: ");
        count = Console.ReadLine();

        Console.WriteLine("autoshit preparing..");

        MessageGetHistoryObject history = vk_api.Messages.GetHistory(new MessagesGetHistoryParams {

            Count = Convert.ToInt32(count),
            PeerId = Convert.ToInt32(peer_id),
        });
        
        Console.WriteLine("autoshit started!");


        foreach (Message message in history.Messages) {

            Dictionary<string, string> headers = new Dictionary<string, string> { };

            headers.Add("peer_id", message.PeerId.ToString());
            headers.Add("reaction_id", "5");
            headers.Add("cmid", message.ConversationMessageId.ToString());
            headers.Add("access_token", vk_api.Token);
            
            string result = await send_post_request($"https://api.vk.com/method/messages.sendReaction?v=5.204", headers);
            Console.WriteLine(result);

            Thread.Sleep(1200); // vk reactions antiflood bypass 2023
        }

        Console.WriteLine("autoshit completed");

        Thread.Sleep(1000);
        Console.Clear();
    }

} while (Console.ReadKey(true).Key != ConsoleKey.Escape);